provider "aws" {
  region = "us-east-1"
}

#Creates a s3 bucket for use for remote state 
module "tf_state_backend" {
   source                             = "git::https://gitlab.com/ricardocg-tf-aws/remote-state-module?ref=master"
   namespace                          = "test"
   stage                              = "test"
   name                               = "ricardocg"
#   attributes                         = ["state"]
   region                             = "us-east-1"
   force_destroy                      = true
#   terraform_backend_config_file_path = "."
#   terraform_backend_config_file_name = "backend.tf"  // Cloudposse module variables
}

resource "aws_sqs_queue" "queue" {
  name                      = "example-queue"
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10

  #fifo_queue                  = true  

  tags = {
    Environment = "production"
  }
}

resource "aws_sns_topic" "topic" {
  name = "example-topic"
}


resource "aws_sns_topic_subscription" "sqs_target" {
  topic_arn = aws_sns_topic.topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.queue.arn
}