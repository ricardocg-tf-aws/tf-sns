output "queue_name" {
    description = "Name of the queue deployed"
    value       = "${aws_sqs_queue.queue.name}"
}

output "sns_name" {
    description = "Name of the SNS deployed"
    value       = "${aws_sns_topic.topic.name}"
}
/*output "records" {
    description = "Name of the records"
    value       = ""  # Investigar como meter un arreglo en un output
}
*/

output "backend" {
    description = "Name of the s3 bucket created for backend"
    value       = "${module.tf_state_backend.bucket_name}"
}